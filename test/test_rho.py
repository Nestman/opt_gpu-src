import numpy as np
from mpi4py.MPI import COMM_WORLD as comm

import sys
sys.path.append('..')
from lib.tool import memory_report
from lib.tool import PML
from lib.operators import rho_step
from lib.gce.space import initialize_space
from lib.gce.grid import Grid

dtype=np.complex128
shape=(100,100,100)

initialize_space(shape)

f_rho=rho_step(dtype)
x=dtype([np.random.randn(*shape),np.random.randn(*shape),np.random.randn(*shape)])

v = [Grid(dtype(np.copy(f)), x_overlap=1) for f in x]
p = [Grid(dtype, x_overlap=1) for k in range(3)]
bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in x]
xg = [Grid(dtype, x_overlap=1) for k in range(3)]
for i in range(40000):
    temp, b_norm = f_rho(0.01, p, bg, v, xg)
    memory_report(str(i)+'-'+str(comm.rank)+'-')
    print 'norm=',b_norm
