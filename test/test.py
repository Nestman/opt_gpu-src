from lib.bicg import bicg
from lib.gce.grid import Grid
import numpy as np
import gc
from mpi4py.MPI import COMM_WORLD as comm
dtype=np.complex128

def solve(b0,x0,maxit,tol,param):
    bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b0]
    xg = [Grid(dtype(f), x_overlap=1) for f in x0]
    
    xg, err, success = bicg(bg, x=xg, \
                            max_iters=maxit, \
                            err_thresh=tol, \
                            **param)

    field=[E.get() for E in xg]
    x0[:]=list(field)

    del(bg)
    del(field)
    
    return err

def test(bg,b):
    a=[np.copy(f) for f in b]
    bg = [Grid(f, x_overlap=1) for f in b]
    bg2 = [Grid(f, x_overlap=1) for f in a]
    b=[E.get() for E in bg]
    b2=[E.get() for E in bg2]

    if comm.rank==0:
        val=b[0][0][0][0]
    else:
        val=0
    return val
