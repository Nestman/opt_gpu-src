#!/bin/bash
#SBATCH --job-name=opt
#SBATCH --output=test.txt
#SBATCH --gres=gpu:2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:30:00
#SBATCH --mem-per-cpu=4000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=coupleropt.py
np=2
Job=-1
freq1=1
freq2=2
Qabs=400
outputfield=0
opt_side=0
with_wvg=1
powerindex=-1.0
bproj=10
R=4
alpha=3.5

hx=0.02
hy=0.02
hz=0.02

mx=.4;
my=.4;
mz=0.16;
gapxz=0.5;
gapy=.5;
wvgt=0.3;
lapy=0.3;

Nx=$(echo "(2*$gapxz+$mx)/$hx" | bc);
Ny=$(echo "(2*$gapy+$my)/$hy" | bc);
Nz=$(echo "(2*$gapxz+$mz)/$hz" | bc);
Mx=$(echo "$mx/$hx" | bc);
My=$(echo "$my/$hy" | bc);
Mz=$(echo "$mz/$hz" | bc);
Mx0=$((($Nx-$Mx)/2))
My0=$((($Ny-$My)/2))
Mz0=$((($Nz-$Mz)/2))
Mzslab=1

Npmlx=10
Npmly=10
Npmlz=10

epsbkg1=1
epssub1=2.0736
epsdiff1=8.3025
epsbkg2=1
epssub2=2.1025
epsdiff2=9.3041
epstype='file'
epsfile='c.txt'

p1=0;
p2=2;
wvg_tx=$(echo "$wvgt/$hx" | bc);
wvg_ty=$Ny
wvg_tz=$Mz
wvg_x0=$((($Nx-$wvg_tx)/2))
wvg_y0=0
wvg_z0=$Mz0

lap_tx=$wvg_tx
lap_ty=1
lap_tz=$wvg_tz
lap_x0=$wvg_x0
lap_y0=$(echo "$Ny-$lapy/$hy" | bc);
lap_z0=$wvg_z0

abs_tx=5
abs_ty=5
abs_tz=$Mz
abs_x0=$((($Nx-$abs_tx)/2))
abs_y0=$((($Ny-$abs_ty)/2))
abs_z0=$Mz0

Jamp=1
Jdir=0
Jopt=1
cx=$(($Nx/2))
cy=$(echo "$lapy/$hy" | bc);
cz=$(($Nz/2))

# solver
maxit=40000
tol=1e-4
verbose=1
init=6
init_type='zero'

# nlopt
maxeval=5000
outputbase=2
solverbase=1000

fpre='c_Mx$Mx-My$My-f$freq1-f$freq2-tol$tol-Q$Qabs-$init_type.'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

eval "mpiexec -n $np python $prop -Job $Job -freq1 $freq1 -freq2 $freq2 -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg1 $epsbkg1 -epsdiff1 $epsdiff1 -epssub1 $epssub1 -epsbkg2 $epsbkg2 -epsdiff2 $epsdiff2 -epssub2 $epssub2 -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -outputbase $outputbase -solverbase $solverbase -maxeval $maxeval -init $init -init_type $init_type -lap_x0 $lap_x0 -lap_y0 $lap_y0 -lap_z0 $lap_z0 -lap_tx $lap_tx -lap_ty $lap_ty -lap_tz $lap_tz -outputfield $outputfield -opt_side $opt_side -with_wvg $with_wvg -wvg_x0 $wvg_x0 -wvg_y0 $wvg_y0 -wvg_z0 $wvg_z0 -wvg_tx $wvg_tx -wvg_ty $wvg_ty -wvg_tz $wvg_tz -abs_x0 $abs_x0 -abs_y0 $abs_y0 -abs_z0 $abs_z0 -abs_tx $abs_tx -abs_ty $abs_ty -abs_tz $abs_tz -powerindex $powerindex -Jopt $Jopt -name $fpre -R $R -alpha $alpha -bproj $bproj >$outputfile"
