#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH --gres=gpu:2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:05:00
#SBATCH --mem-per-cpu=2500
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=fdfd.py
np=2
Job=1
freq=1
Qabs=1e16

Nx=100
Ny=100
Nz=100
Mx=50
My=50
Mz=50
Mzslab=1

hx=0.02
hy=0.02
hz=0.02

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epsdiff=5
epstype='vac'
epsfile='rand.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))

# solver
maxit=10000
tol=1e-5
verbose=1
init=2
init_type='zero'

# nlopt
maxeval=5

outputbase=2
solverbase=1000

mpiexec -n $np python $prop -Job $Job -freq $freq -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -outputbase $outputbase -solverbase $solverbase -maxeval $maxeval -init $init -init_type $init_type
