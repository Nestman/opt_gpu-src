#!/bin/bash
#SBATCH --job-name=opt
#SBATCH --output=test.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:30:00
#SBATCH --mem-per-cpu=4000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=shgopt.py
np=1
Job=0
freq1=1
freq2=2
Qabs=200
outputfield=0
powerindex=-1

hx=0.02
hy=0.02
hz=0.02

mx=.4;
my=.4;
mz=0.16;
gap=0.4;

Nx=$(echo "(2*$gap+$mx)/$hx" | bc);
Ny=$(echo "(2*$gap+$my)/$hy" | bc);
Nz=$(echo "(2*$gap+$mz)/$hz" | bc);
Mx=$(echo "$mx/$hx" | bc);
My=$(echo "$my/$hy" | bc);
Mz=$(echo "$mz/$hz" | bc);
Mx0=$((($Nx-$Mx)/2))
My0=$((($Ny-$My)/2))
Mz0=$((($Nz-$Mz)/2))
Mzslab=1

Npmlx=10
Npmly=10
Npmlz=10

epsbkg1=1
epssub1=2.0736
epsdiff1=8.3025
epsbkg2=1
epssub2=2.1025
epsdiff2=9.3041
epstype='one'
epsfile='pad1_cavity.txt'

p1=0;
p2=2;

Jamp=10
Jdir=0
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))

# solver
maxit=70000
tol=1e-4
verbose=1
init=6
init_type='zero'
solverbase=1000

# nlopt
maxeval=5000
outputbase=2

fpre='shg_Mx$Mx-My$My-Mz$Mz-f$freq1-f$freq2-tol$tol-Q$Qabs-p$powerindex.'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

eval "mpiexec -n $np python $prop -Job $Job -freq1 $freq1 -freq2 $freq2 \
-Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz \
-Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -Mzslab $Mzslab \
-hx $hx -hy $hy -hz $hz \
-Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz \
-epsbkg1 $epsbkg1 -epsdiff1 $epsdiff1 -epssub1 $epssub1 \
-epsbkg2 $epsbkg2 -epsdiff2 $epsdiff2 -epssub2 $epssub2 \
-epstype $epstype -epsfile $epsfile \
-Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz \
-maxit $maxit -tol $tol -verbose $verbose -outputbase $outputbase \
-solverbase $solverbase -maxeval $maxeval -init $init -init_type $init_type \
-outputfield $outputfield -name $fpre \
-p1 $p1 -p2 $p2 -powerindex $powerindex \
>$outputfile"
